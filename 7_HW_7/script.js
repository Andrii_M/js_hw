// variant 1
function filterBy(array, type) {
  return array.reduce(function(res, currentItem) {
    if (typeof currentItem != type) {
      res.push(currentItem);
    }
    return res;
  }, []);
}
console.log(filterBy(["hello", "world", 23, "23", null], "string"));

//variant 2
// function filterBy(array, type) {
//   return array.reduce((res, currentItem) => {
//     if (typeof currentItem != type) {
//       res.push(currentItem);
//     }
//     return res;
//   }, []);
// }
// console.log(filterBy(["hello", "world", 23, "23", null], "string"));

//variant 3
// let items = ['hello', 'world', 23, '23', null]

// const filterBy = (arr, type) => arr.filter(item => typeof item !== type)

// console.log(filterBy(items, "string"))

//variant 4
// let testArr = ['hello', 'world', 23, '23', null];
// function filterByTest(arr, type) {
//     return arr.filter(function (item) {
//         return typeof item !== type;
//     });
// }
// console.log(filterByTest(testArr, "string"))
