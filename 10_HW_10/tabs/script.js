// Завдання
// Реалізувати перемикання вкладок (таби) на чистому Javascript.

// Технічні вимоги:

// 1. У папці tabs лежить розмітка для вкладок. Потрібно, щоб після 
// натискання на вкладку відображався конкретний текст для потрібної вкладки. 
// При цьому решта тексту повинна бути прихована. У коментарях зазначено, 
// який текст має відображатися для якої вкладки.

// 2. Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.

// 3. Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися 
// та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки 
// не переставала працювати.

//1 функцыя онклик плей ин джс, табс-тайтл выдслыдковувати онклик.ивент потым запуск ивент.хендлер
//функция видалити клас актив из табс-тайтл и потым присвоъти актив через евент.таргет
//delete class active-content з усих li tabs-content 
//витянути из дата елемент идентификатор ли и для такого додати актив-контент



const tabsBTN = document.querySelectorAll(".nav-tab");
const tabsItems = document.querySelectorAll(".tab-pane");

tabsBTN.forEach(function(item) {
    item.addEventListener("click", function(){
        let currentBTN = item;
        let tabId = currentBTN.getAttribute("data-tab");
        let currentTab = document.querySelector(tabId);

        if (! currentBTN.classList.contains('active')){
            tabsBTN.forEach(function(item) {
                item.classList.remove('active');
            });
    
            tabsItems.forEach(function(item) {
                item.classList.remove('active');
            });
    
            currentBTN.classList.add('active');
            currentTab.classList.add('active');

        }
    });
});

