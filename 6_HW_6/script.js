function createNewUser() {
    let a = prompt("Enter first name")
    while (!a) {
        a = prompt("Enter first name")
    }

    let b = prompt("Enter last name")
    while (!b) {
        b = prompt("Enter last name")
    }

    let c = prompt("Enter your date of birth (dd.mm.yyyy)")

    let newUser = {
        firstName: a,
        lastName: b,
        birthday: c, 
        getLogin: function() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        getAge: function(){
            let splitDate = c.split(".")
            let currentDate = new Date()
            let userDate = new Date(splitDate[2], splitDate[1]-1, splitDate[0])
            return currentDate.getFullYear()-userDate.getFullYear()
        },
        getPassword: function(){
            let splitDate = c.split(".")
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + splitDate[2]
        }
    }
    return newUser
}
let user = createNewUser()
console.log(user.getLogin())
console.log(user.getAge())
console.log(user.getPassword())
