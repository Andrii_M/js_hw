// Отримати за допомогою модального вікна браузера число, яке введе користувач.
// За допомогою функції порахувати факторіал числа, яке ввів користувач і вивести його на екран.
// Використовувати синтаксис ES6 для роботи зі змінними та функціями.

//варіант 1
let n = +prompt('Enter your number')
while (!n || isNaN(n)) {
    n = +prompt('Enter your number')
}

function factorial(n){
    let result = 1;
    while(n){
        result *= n--;
    }
    return result;
}
alert(factorial(n))




//варіант 2 (рекурсія)
// let n = +prompt('Enter your number')
// while (!n || isNaN(n)) {
//     n = +prompt('Enter your number')
// }

// function factorial(n, result){
//     result = result || 1;
//     if(!n){
//         return result;
//     }else{
//         return factorial(n-1, result*n);
//     }
// }
// alert(factorial(n))