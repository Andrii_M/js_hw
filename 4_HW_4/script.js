let firstNum = +prompt("Введите первое число") // закоментувати при використанні рекрсії рекурсії
while (!firstNum || isNaN(firstNum))  {        // закоментувати при використанні рекрсії рекурсії
    firstNum = +prompt('Введите первое число');// закоментувати при використанні рекрсії рекурсії
 }

 // let firstNum = getNum("Введите первое число") // використати для рекурсії
 
let secondNum = +prompt("Введите второе число")// закоментувати при використанні рекрсії рекурсії
while (!secondNum || isNaN(secondNum))  {      // закоментувати при використанні рекрсії рекурсії
    secondNum = +prompt('Введите второе число');// закоментувати при використанні рекрсії рекурсії
}

// let secondNum = getNum("Введите второе число") // використати для рекурсії

let operation = prompt("Введите операцию: сума, разница, умножение, деление")
while (operation != '-' && operation != '+' && operation != '*' && operation != '/') {
    operation = prompt('Введите операцию: сума, разница, умножение, деление');
}

//рекурсія 

// function getNum(message) {
//     let num = +prompt(message);
//     if (!num || isNaN(num)) {
//         getNum(message);
//     }
//     return num;
// }

// варіант 1
function calc(a, b, operation) {
    if(operation === '-') {
        return a - b
    }
    if(operation === '+'){
        return a + b 
    }
    if(operation === '*'){
        return a * b 
    }
    if(operation === '/'){
        return a / b 
    }
}

// варіант 2
function calc1(a, b, operation){
    let result = null
    switch (operation) {
        case '-':
            result = a - b;
            break;
        case '+':
            result = a + b;
            break;
        case '*':
            result = a * b;
            break;
        case '/':
            result = a / b;
            break;
        default: 
            break;
    }
    return result
}

console.log(calc(firstNum, secondNum, operation))
console.log(calc1(firstNum, secondNum, operation))
