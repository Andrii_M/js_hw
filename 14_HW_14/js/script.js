let colorArray = [ "#f9ff33", "#fff"]; // массив с цветами
let i = 0; // итератор

function changeColor() {
    let currentColor = colorArray[i];
    document.body.style.background = currentColor;
    localStorage.setItem('backgroundColor', currentColor);
    i++;
    if( i > colorArray.length - 1){
        i = 0;
    }
}

window.onload = function() {
    document.body.style.background = localStorage.getItem('backgroundColor');
  };
