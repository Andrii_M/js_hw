

// 1 Знайти всі параграфи на сторінці та встановити колір фону #ff0000

document.querySelectorAll('p').forEach(e => e.style.backgroundColor = '#ff0000');

//2 Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський 
// елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, 
// і вивести в консоль назви та тип нод.

let optionsList = document.getElementById("optionsList")

console.log('option list', optionsList)

console.log('option list parent node', optionsList.parentNode)

if (optionsList.hasChildNodes()) {

  let children = optionsList.childNodes;

  for (let i = 0; i < children.length; ++i) {
    console.log(children[i].nodeType)
  }
}
// Не зрозуміло , що малось на увазі "вивести в консоль назви та тип нод" ???

//3 Встановіть в якості контента елемента з класом testParagraph наступний параграф - 
// This is a paragraph

let testParagraph = document.getElementById("testParagraph")

testParagraph.textContent = 'This is a paragraph'

//4,5 Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
//Кожному з елементів присвоїти новий клас nav-item.

let mainHeader = document.getElementsByClassName("main-header")[0]

if (mainHeader.hasChildNodes()) {

  let children = mainHeader.childNodes;

  for (let i = 0; i < children.length; ++i) {
    console.log('main-header children ' + i, children[i])
    if (children[i].classList){
      children[i].classList.add("nav-item");
    }
  }
}

//6 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let sectionTitle = document.querySelectorAll(".section-title")

sectionTitle.forEach(e => e.classList.remove("section-title"))










