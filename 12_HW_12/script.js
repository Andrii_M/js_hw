const buttons = document.querySelectorAll(".btn")
document.addEventListener("keyup", (e) => {
    buttons.forEach(button => {
        button.style.backgroundColor = "black"
        if (e.key === button.textContent || e.key === button.textContent.toLowerCase()){
            button.style.backgroundColor = "blue"
        }
    })
})
let clickBtn;
document.addEventListener('click', (event) => {
  if(event.target !== event.currentTarget && event.target.classList.contains('btn')) {
    if(clickBtn) clickBtn.classList.remove('blue');
    event.target.classList.add('blue');
    clickBtn = event.target;
}
})

